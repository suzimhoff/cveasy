from django.urls import path
from . import views


urlpatterns = [
    path('/formular', views.formular, name="formular"),
]